package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Beans.UserBeans;
import base.DBManager;

public class UserDAO {

  public UserBeans findByloginInfo(String email, String enPassword) {

    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM todolist_user WHERE password=? and email=?";
      // SELECT分の実行と結果の取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, enPassword);
      pStmt.setString(2, email);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("user_id");
      String userName = rs.getString("user_name");
      String userPassword = rs.getString("password");
      String userEmail = rs.getString("email");

      return new UserBeans(id, userName, userPassword, userEmail);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

}
