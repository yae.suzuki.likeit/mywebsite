package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Beans.TodolistBeans;
import base.DBManager;

public class TodolistDAO {
  
  //todoリストの一覧表を取得
  public List<TodolistBeans> findListAll(String email) {
    Connection conn = null;
    List<TodolistBeans> todoList = new ArrayList<TodolistBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT * FROM todolist_user JOIN todolist ON todolist.user_id = todolist_user.user_id  WHERE email =? ORDER BY todlist_id DESC";

      // SELECTを実行し、結果表を取得
      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1,email);
      ResultSet rs = stmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int todolistId = rs.getInt("todlist_id");
        String todolistTitle = rs.getString("todolist_title");
        int userId = rs.getInt("user_id");
        
        TodolistBeans todolist = new TodolistBeans(todolistId, todolistTitle, userId);
        
        todoList.add(todolist);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;
  }

  public TodolistBeans findBytodolistId(int todolistId) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();
      String sql = "SELECT * FROM todolist WHERE todlist_id = ?";

      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setInt(1, todolistId);
      ResultSet rs = stmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int todolist_id = rs.getInt("todlist_id");
      String todolistTitle = rs.getString("todolist_title");
      int user_id = rs.getInt("user_id");

      // todolistIdを基にtodolistのレコードを取得
      return new TodolistBeans(todolist_id, todolistTitle, user_id);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }


  }

  public List<TodolistBeans> research(String title, int user_id) {
    
    Connection conn = null;
    List<TodolistBeans> todoList = new ArrayList<TodolistBeans>();

    try {
      conn = DBManager.getConnection();
      String sql =
          "SELECT * FROM todolist WHERE todolist_title LIKE ? AND user_id=? ORDER BY todlist_id DESC";
      
      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1, "%" + title + "%");
      stmt.setInt(2, user_id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int todolistId = rs.getInt("todlist_id");
        String todolistTitle = rs.getString("todolist_title");
        int userId = rs.getInt("user_id");

        TodolistBeans todolist = new TodolistBeans(todolistId, todolistTitle, userId);


        todoList.add(todolist);
        
      }
      

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return todoList;
  }

  public void insert(String todolistTitle, int userId) {

    Connection conn = null;
    try {
      conn = DBManager.getConnection();

      String sql = "INSERT INTO todolist(todolist_title,user_id) VALUES(?,?)";

      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1, todolistTitle);
      stmt.setInt(2, userId);
      // SQLの実行
      stmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }


  }
}

public void update(int todolistId,String updateTodolistTitle) {
    
    Connection conn = null;
    try {
      conn =DBManager.getConnection();
      String sql = "UPDATE todolist SET todolist_title=? WHERE todlist_id=? ";

      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1, updateTodolistTitle);
      stmt.setInt(2, todolistId);

      // SQLの実行
      stmt.executeUpdate();

      
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }

    }
  }

  public void delete(int todolistId) {

    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      String sql = "DELETE FROM todolist WHERE todlist_id = ? ";

      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setInt(1, todolistId);
      // SQLの実行
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }

    }

  }
  }



