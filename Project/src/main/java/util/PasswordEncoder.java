package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncoder {

  String encodeStr;

  /* public static void main(String[] args) { */

  public String encordPassword(String password) {

    // 引数で渡されたパスワードを読み取る
    // ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    // ハッシュ関数の種類(今回はMD5)
    String algorithm = "MD5";

    // ハッシュ生成処理
    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));

      String encodeStr = DatatypeConverter.printHexBinary(bytes);
      // encodeStrよりもencodePassの方が名前としては良い

      // 暗号化結果の出力
      // System.out.println(encodeStr);
      // passwordに暗号化した結果を代入する
      password = encodeStr;

      return encodeStr;



    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return null;
    }

  }
}


