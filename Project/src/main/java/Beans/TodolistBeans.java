package Beans;

public class TodolistBeans {

  private int todolistId;
  private String todolistTitle;
  private int userId;

  public TodolistBeans() {

  }

  public TodolistBeans(int todolistId, String todolistTitle, int userId) {
    this.setTodolistId(todolistId);
    this.setTodolistTitle(todolistTitle);
    this.setUserId(userId);
  }

  public int getTodolistId() {
    return todolistId;
  }

  public void setTodolistId(int todolistId) {
    this.todolistId = todolistId;
  }

  public String getTodolistTitle() {
    return todolistTitle;
  }

  public void setTodolistTitle(String todolistTitle) {
    this.todolistTitle = todolistTitle;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

}
