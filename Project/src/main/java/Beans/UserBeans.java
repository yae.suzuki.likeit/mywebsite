package Beans;

public class UserBeans {

  private int id;
  private String userName;
  private String userPassword;
  private String userEmail;


  public UserBeans() {

  }

  public UserBeans(int id, String userName, String userPassword, String userEmail) {

    this.setId(id);
    this.setUserName(userName);
    this.setUserPassword(userPassword);
    this.setUserEmail(userEmail);

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPassword() {
    return userPassword;
  }

  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }
}
