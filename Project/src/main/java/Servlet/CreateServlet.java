package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.UserBeans;
import dao.TodolistDAO;

/**
 * Servlet implementation class create
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // ログインチェック
        HttpSession session = request.getSession();
        UserBeans user = (UserBeans) session.getAttribute("user");

        if (user == null) {
          response.sendRedirect("LoginServlet");

        }
        // todoリストの新規追加画面に遷移

        request.setAttribute("user", user);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
        dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        // 入力された新しいデータとユーザーIDをパラメータとして受け取る
        String todolistTitle = request.getParameter("title");
        int userId = Integer.valueOf(request.getParameter("id"));

        // titleが未入力の場合にエラーを出す
        if (todolistTitle.equals("")) {

          // エラー文をセット
          request.setAttribute("errMsg", "入力された内容は正しくありません");
          // create画面に遷移する
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
          dispatcher.forward(request, response);
        } else {
        // todoリストに追加するメソッドにtodlokistTitleとemailを引数にして渡す


        TodolistDAO todolistDao = new TodolistDAO();
        todolistDao.insert(todolistTitle, userId);

        // 追加後、一覧サーブレットを経由後、一覧画面へ
        response.sendRedirect("ListServlet");

      }



	}

}
