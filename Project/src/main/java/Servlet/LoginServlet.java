package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.UserBeans;
import dao.UserDAO;
import util.PasswordEncoder;

/**
 * Servlet implementation class login
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // ログイン画面に遷移
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);

	}

      /*
       * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
       */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 文字コードの指定
        request.setCharacterEncoding("UTF-8");

        // メールアドレスとパスワードのパラメータを受け取る
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // 受け取ったパスワードを暗号化する
        PasswordEncoder passEncoder = new PasswordEncoder();
        String enPassword = passEncoder.encordPassword(password);
        // ※下記で暗号化されたパスワードをコンソールで取得し、DBでアップデートする※
        System.out.println(enPassword);

        // リクエストパラメータを引数にログインチェックを行なう
        UserDAO userDao = new UserDAO();
        UserBeans user = userDao.findByloginInfo(email, enPassword);
        //userがnullの場合エラーを提示する
        if(user == null) {
          request.setAttribute("errMsg", "ログインに失敗しました");
          
          //ログインjspにフォワード
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
          dispatcher.forward(request, response);
        }
        // セッションにユーザーの情報をセット
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        // 一覧表を提示するためにメールアドレスをセッションにセット
        session.setAttribute("userEmail", email);

        // 一覧サーブレットにリダイレクト
        response.sendRedirect("ListServlet");
	}

}
