package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.TodolistBeans;
import Beans.UserBeans;
import dao.TodolistDAO;

/**
 * Servlet implementation class update
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        // ログインチェック
        HttpSession session = request.getSession();
        UserBeans user = (UserBeans) session.getAttribute("user");

        if (user == null) {
          response.sendRedirect("LoginServlet");
          return;
        }

        // todolistIdと更新されのパラメータを受け取る
        int todolistId = Integer.valueOf(request.getParameter("todolistId"));

        // todolistIdを引数にしてそのレコードを取得
        TodolistDAO todolistDao = new TodolistDAO();
        TodolistBeans todo = todolistDao.findBytodolistId(todolistId);

        // todolistをセットして更新画面に遷移する
        request.setAttribute("todo", todo);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
        dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


      request.setCharacterEncoding("UTF-8");

        // todolistIdと更新されのパラメータを受け取る
        int todolistId = Integer.valueOf(request.getParameter("id"));
        int todolistUserId = Integer.valueOf(request.getParameter("userId"));
        String updateTodolistTitle = request.getParameter("updateTitle");

        // updateTodolistTitleが未入力の場合エラー文を出力
        if (updateTodolistTitle.equals("")) {
          request.setAttribute("errMsg", "入力された内容は正しくありません");
          request.setAttribute("title", updateTodolistTitle);

          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
          dispatcher.forward(request, response);
        } else {


        // 受け取ったIdを引数にしてtodolistの更新を行なう
        TodolistDAO todolistDao = new TodolistDAO();
        todolistDao.update(todolistId, updateTodolistTitle);

        // 入力された内容をセットする


        // listServletを経由して一覧画面に戻る
        response.sendRedirect("ListServlet");
      }



	}

}
