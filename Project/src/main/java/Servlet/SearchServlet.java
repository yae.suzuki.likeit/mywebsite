package Servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Beans.TodolistBeans;
import dao.TodolistDAO;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        // 検索欄から受け取ったパラメータとユーザーIDを取得する
        String title = request.getParameter("title");
        int user_id = Integer.valueOf(request.getParameter("id"));

        // 受け取ったtitleとユーザーIDを引数にして検索メソッドを使いレコードを取得

        TodolistDAO todolistDao = new TodolistDAO();
        List<TodolistBeans> todoList = todolistDao.research(title, user_id);

        // todoListをセットしてListServletに遷移する
        request.setAttribute("todoList", todoList);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
        dispatcher.forward(request, response);


	}

}
