package Servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.TodolistBeans;
import Beans.UserBeans;
import dao.TodolistDAO;

/**
 * Servlet implementation class list
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        // セッションからユーザー情報を取得→ログインチャック

        HttpSession session = request.getSession();
        UserBeans user = (UserBeans) session.getAttribute("user");


        // ユーザー情報がnullの場合ログイン画面へ遷移
        if (user == null) {
          response.sendRedirect("LoginServlet");
          return;
        }

        String email = (String) session.getAttribute("userEmail");
        // emailを引数にユーザーのtodoリスト一覧を取得
        TodolistDAO todolistDao = new TodolistDAO();
        List<TodolistBeans> todoList = todolistDao.findListAll(email);

        // リクエストスコープにtodoリスト一覧情報とユーザー情報をセット
        request.setAttribute("todoList", todoList);
        request.setAttribute("user", user);
        // リスト一覧画面へ遷移
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/list.jsp");
        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
	}

}
