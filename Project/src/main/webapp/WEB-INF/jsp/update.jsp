<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TODO App | 更新</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
    <!-- FontAwesomeの読み込み-->
    <script src="https://kit.fontawesome.com/f4ccc568fd.js" crossorigin="anonymous"></script>
</head>

<body class="bg-light">
    <!-- header -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
            <div class="container">
                <a class="navbar-brand" href="ListServlet">
                    <!-- ナビゲーションバーの「TODO」 -->
                    <i class="fa-solid fa-t"></i>
                    <i class="fa-solid fa-o"></i>
                    <i class="fa-solid fa-d"></i>
                    <i class="fa-solid fa-o"></i>

                    <i class="fa-solid fa-a"></i>
                    <i class="fa-solid fa-p"></i>
                    <i class="fa-solid fa-p"></i>
                </a>

                <div class="d-flex">
                    <ul class="navbar-nav">
                        <li class="nav-item navbar-text">
                            ${user.userName}さん</li>
                        <li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- /header -->

    <!-- body -->
    <div class="container">

        <div class="d-flex justify-content-center align-self-center">
            <div class="card" style="width: 600px;">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                </c:if>
                <!--エラーメッセージ  end  -->

                <div class="card-body">
                    <!-- 更新用フォーム-->
                    <form method="post" action="UpdateServlet">
                        <!-- TODOのID（hiddenタグ）-->
                        <input type="hidden" name="id" value="${todo.todolistId}">
                        <input type="hidden" name="userId" value="${todo.userId}">

                        <!-- TODOの入力エリア-->
                        <div class="mb-3 row">
                            <div class="input-group">
                                <span class="input-group-text" id="todo-addon"> <i class="fa-solid fa-check"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="input your TODO" aria-label="todo"
                                    aria-describedby="todo-addon" name="updateTitle" value="${todo.todolistTitle}">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col d-grid gap-2">
                                <a class="btn btn-secondary" href="ListServlet" type="button">戻る</a>
                            </div><!-- 更新ボタン-->
                            <div class="col d-grid gap-2">
                                <button class="btn btn-primary" type="submit">更新</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
