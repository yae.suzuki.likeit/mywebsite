CREATE DATABASE todolist DEFAULT CHARACTER SET utf8;

USE todolist;

CREATE TABLE todolist_user(
user_id int PRIMARY KEY AUTO_INCREMENT,
user_name varchar(30) NOT NULL,
password varchar(30) NOT NULL,
email varchar(40) NOT NULL);

INSERT INTO todolist_user(user_name,password,email)
 VALUES('鈴木たろう','password','marimari@gmail.jp');



USE todolist;

CREATE TABLE todolist(
todlist_id int PRIMARY KEY AUTO_INCREMENT,
todolist_title varchar(150) NOT NULL,
user_id int NOT NULL,
INDEX user_id_index(user_id),
FOREIGN KEY fk_user_id(user_id) REFERENCES todolist_user(user_id));


INSERT INTO todolist(todolist_title,user_id)
 VALUES('ごはんたべる',1);
